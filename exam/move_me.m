function B= move_me(A)
e=input('e=');
B = e*ones(size(A));
h=A~=e; 
B(6-sum(h):end) = A(h);
end
