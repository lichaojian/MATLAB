function Q=intquad(n)
H=ones(n);
Q=[H*-1,H*exp(1);H*pi,H];
end